//chokidar ??
require('shelljs/global')
console.info("check typescript Version")
exec(
    'tsc -v', (status, output) => {
        if (output.indexOf("Version") != -1) {
            compile()
        } else {
            console.log("install typescript")
            installTS()
        }
    }
)


const installTS = () => exec(
    'npm install -g typescript', (status, output) => {
        console.log("typescript installed")
        compile()
    }
)

const compile = () => {
    console.log("start compilation")
    exec(
        'tsc', (status, output) => {
            console.log(output)
            console.log("compile done, starting app")
            if (status != 1) require("root/start")
        }
    )
}

